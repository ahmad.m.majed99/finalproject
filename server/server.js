import express from "express";
import dotenv from "dotenv";
import connectDB from "./config/db.js";
import userRoutes from "./routes/userRoutes.js";
import bodyParser from "body-parser";
import cors from "cors";
dotenv.config();

connectDB();

const PORT = process.env.PORT || 5000;
const app = new express();
// console.log("hello nodejs");
// Multerr
app.use("/images", express.static("./images"));
app.use(cors());
app.use(express.json());
app.use(bodyParser.urlencoded({ extended: false }));

// parse application/json
app.use(bodyParser.json());
app.use("/api/users", userRoutes);

app.listen(PORT, console.log(`Server Running on Port ${PORT}`));
