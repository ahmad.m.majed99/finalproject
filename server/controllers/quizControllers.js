import asyncHandler from "express-async-handler";

// import User from "../models/userModel.js";

const quizControllers = asyncHandler(async (req, res) => {
  const { age, height, weight, goal, gender, sport, food } = req.body;

  var BMI = (weight / Math.pow(height / 100, 2)).toFixed(1);

  const theBMI = () => {
    if (BMI < 18.5) {
      return "underweight";
    } else if (BMI >= 18.5 && BMI <= 24.9) {
      return "normalweight";
    } else if (BMI >= 25 && BMI <= 29.9) {
      return "overweight";
    } else if (BMI >= 30) {
      return "obese";
    }
  };

  const theGender = () => {
    if (gender === "Male") return true;
    if (gender === "Female") return false;
  };

  const theGoal = () => {
    switch (goal) {
      case "LoseWeight":
        return "LoseWeight";

      case "GainMuscles":
        return "GainMuscles";

      case "FitBody":
        return "FitBody";
    }
  };

  const theAge = () => {
    if (age >= 20 && age <= 30) {
      return 2030;
    }
    if (age > 30 && age <= 40) {
      return 3040;
    }
    if (age > 40 && age <= 50) {
      return 4050;
    }
  };

  const theSport = () => {
    switch (sport) {
      case "noTrain":
        return "noTrain";
      case "train":
        return "train";
    }
  };

  const theVegan = () => {
    switch (food) {
      case "vegan":
        return true;
      case "notVegan":
        return false;
    }
  };

  const pathVegan = "images/VeganProgram.png";
  const pathMe = "images/program1.png";

  let BMW = theBMI(BMI);

  if (
    theGender() === true &&
    theGoal() === "LoseWeight" &&
    theAge() === 2030 &&
    theSport() === "train" &&
    theVegan() === false
  ) {
    console.log(BMI, BMW);
    res.json({
      program: pathMe,
      BMI: BMI,
      type: BMW,
    });
  }
  if (
    theGender() === true &&
    theGoal() === "GainMuscles" &&
    theAge() === 2030 &&
    theSport() === "train" &&
    theVegan() === false
  ) {
    res.json({
      program: pathMe,
      BMI: BMI,
      type: BMW,
    });
  }
  if (
    theGender() === true &&
    theGoal() === "FitBody" &&
    theAge() === 2030 &&
    theSport() === "train" &&
    theVegan() === false
  ) {
    res.json({
      message: "program 3",
    });
  }
  if (
    theGender() === true &&
    theGoal() === "LoseWeight" &&
    theAge() === 3040 &&
    theSport() === "train" &&
    theVegan() === false
  ) {
    res.json({
      message: "program 4",
    });
  }
  if (
    theGender() === true &&
    theGoal() === "LoseWeight" &&
    theAge() === 4050 &&
    theSport() === "train" &&
    theVegan() === false
  ) {
    res.json({
      message: "program 5",
    });
  }
  if (
    (theGender() === false &&
      theGoal() === "LoseWeight" &&
      theAge() === 2030) ||
    (3040 && theSport() === "train" && theVegan() === true)
  ) {
    res.json({
      program: pathVegan,
      BMI: BMI,
      type: BMW,
    });
  }
});

export { quizControllers };
