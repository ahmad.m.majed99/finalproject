import express from "express";
const router = express.Router();
import { authUser, registerUser } from "../controllers/userControllers.js";

import { quizControllers } from "../controllers/quizControllers.js";

import { protect } from "../middleware/authMiddleware.js";

router.route("/register").post(registerUser).get(protect);
router.post("/login", authUser);

router.post("/questionary", quizControllers);

export default router;
