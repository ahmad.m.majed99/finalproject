import React, { useRef, useState } from "react";
import { FaBars, FaTimes } from "react-icons/fa";
import "./Navbar.css";

const Navbar = () => {
  const [modal1, setModal1] = useState(false);
  const [modal2, setModal2] = useState(false);

  const toggleModal1 = () => {
    setModal1(!modal1);
  };

  const toggleModal2 = () => {
    setModal2(!modal2);
  };

  const navRef = useRef();

  const showNavbar = () => {
    navRef.current.classList.toggle("responsive_nav");
  };

  return (
    <>
      <div className="header">
        <h4>BETTER ME.</h4>
        <div className="nav" ref={navRef}>
          {/* <a href="/">Home</a>
          <a href="/exercice">Exercice</a>
          <a href="/community">Community</a>
          <a href="/pricing">Pricing</a> */}
          <button className="nav-btn nav-close-btn" onClick={showNavbar}>
            <FaTimes />
          </button>
        </div>
        <button className="nav-btn" onClick={showNavbar}>
          <FaBars />
        </button>
      </div>
    </>
  );
};

export default Navbar;
