import React from "react";
import "./Footer.css";

const Footer = () => {
  return (
    <div className="footer-container">
      <div className="footer-col">
        <h4>Menu</h4>
        <ul>
          <li>
            <a href="#home">Home</a>
          </li>
          <li>
            <a href="#">Pricing</a>
          </li>
          <li>
            <a href="#community">Community</a>
          </li>
          <li>
            <a href="#program">Program</a>
          </li>
        </ul>
      </div>
      <div className="footer-col">
        <h4>Services</h4>
        <ul>
          <li>
            <a href="#">Strength</a>
          </li>
          <li>
            <a href="#">Physical Fitness</a>
          </li>
          <li>
            <a href="#">Fat Loss</a>
          </li>
          <li>
            <a href="#">Weight Gain</a>
          </li>
        </ul>
      </div>
      <div className="footer-col">
        <h4>Pricing</h4>
        <ul>
          <li>
            <a href="#">Daily Plan</a>
          </li>
          <li>
            <a href="#">Weekly Plan</a>
          </li>
          <li>
            <a href="#">Monthly Plan</a>
          </li>
        </ul>
      </div>
      <div className="footer-col">
        <h4>Follow us</h4>
        <div className="social-links">
          <a href="#">
            <i className="fab fa-facebook-f"></i>
          </a>
          <a href="#">
            <i className="fab fa-twitter"></i>
          </a>
          <a href="#">
            <i className="fab fa-instagram"></i>
          </a>
          <a href="#">
            <i className="fab fa-linkedin-in"></i>
          </a>
        </div>
      </div>
    </div>
  );
};

export default Footer;
