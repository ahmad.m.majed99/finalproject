import React from "react";
import "./HorizontallScrollbar.css";
import { Box } from "@mui/material";

const HorizontallScrollbar = ({ data }) => {
  console.log(data);
  return (
    <>
      <div className="hor-cont ">
        {data.map((item) => {
          <Box
            key={item.id || item}
            itemId={item.id || item}
            title={item.id || item}
            m="0 40px"
          >
            {item}
          </Box>;
        })}
      </div>
    </>
  );
};

export default HorizontallScrollbar;
