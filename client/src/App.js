import React from "react";
import { BrowserRouter as Router, Route, Routes } from "react-router-dom";

import Home from "./pages/Home/Home";
import Exercice from "./pages/Exercices/Exercice";

import Questionary from "./pages/Questionary/Questionary";

function App() {
  return (
    <>
      <Router>
        <Routes>
          <Route path="/" element={<Home />} />
          <Route path="/questionary" element={<Questionary />} />
          {/* <Route path="/exercice" element={<Exercice />} /> */}
        </Routes>
      </Router>
    </>
  );
}

export default App;
