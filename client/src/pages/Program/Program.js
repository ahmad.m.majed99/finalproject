import React from "react";
import "./Program.css";

import { BiDumbbell } from "react-icons/bi";
import { IoFitness } from "react-icons/io5";
import { IoBody } from "react-icons/io5";
import { GiBiceps } from "react-icons/gi";

const Program = () => {
  return (
    <div className="program-container" id="program">
      <div className="prg-title">
        <div className="prg-title1">EXPLORE OUR PROGRAM</div>
        <div className="prg-title2">PROGRAM</div>
      </div>
      <div className="program-slider">
        <div className="prog-box box1">
          <div className="prog-icon">
            <BiDumbbell className="icon" />
          </div>
          <div className="prog-subtitle">Strength</div>
          <p className="prog-desc">
            Increasing physical strength is the goal of strength training
          </p>
          <button className="prog-btn">
            Join Now <hr className="prog-hr" />
          </button>
        </div>
        <div className="prog-box box2">
          <div className="prog-icon">
            <IoFitness className="icon" />
          </div>
          <div className="prog-subtitle">Physical Fitness</div>
          <p className="prog-desc">
            Maintain body fitness by doing physical exercice at least 2-3 imes a
            week
          </p>
          <button className="prog-btn">
            Join Now <hr className="prog-hr" />
          </button>
        </div>
        <div className="prog-box box3">
          <div className="prog-icon">
            <IoBody className="icon" />
          </div>
          <div className="prog-subtitle">Fat Lose</div>
          <p className="prog-desc">
            Aims to reduce fat as much as possible from the body to about 2-4%
          </p>
          <button className="prog-btn">
            Join Now <hr className="prog-hr" />
          </button>
        </div>
        <div className="prog-box box4">
          <div className="prog-icon">
            <GiBiceps className="icon" />
          </div>
          <div className="prog-subtitle">Weight Gain</div>
          <p className="prog-desc">
            Focus on increasing the number of reps or increasing the load
          </p>
          <button className="prog-btn">
            Join Now <hr className="prog-hr" />
          </button>
        </div>
      </div>
    </div>
  );
};

export default Program;
