import React, { useState, useEffect } from "react";
import HorizontallScrollbar from "../../components/HorizontallScrollbar/HorizontallScrollbar";
import "./Exercice.css";
// import { Box, Button, Stack, TextField, Typography } from "@mui/material";

const Exercice = () => {
  const [search, setSearch] = useState("");
  const [exercises, setExercises] = useState([]);
  const [bodyParts, setBodyParts] = useState([]);

  const options = {
    method: "GET",
    headers: {
      "X-RapidAPI-Key": "6b7b9819dfmsheb4aff9c7dd5073p11b048jsnd35cecde77f4",
      "X-RapidAPI-Host": "exercisedb.p.rapidapi.com",
    },
  };

  useEffect(() => {
    const fetchExercisesData = async () => {
      const bodyPartsData = await fetch(
        "https://exercisedb.p.rapidapi.com/exercises/bodyPartList",
        options
      );

      console.log(bodyPartsData.json());
      // setBodyParts(["all", ...bodyPartsData]);
      // setBodyParts(bodyPartsData);
      // console.log(bodyParts);
    };

    fetchExercisesData();
  }, []);

  const handleSearch = async () => {
    try {
      if (search) {
        const exercisesData = await fetch(
          "https://exercisedb.p.rapidapi.com/exercises",
          options
        );
        const data = exercisesData.json();

        console.log(data);

        setSearch("");
      }
    } catch (err) {
      console.log(err);
    }
  };

  return (
    <>
      <div className="search-container">
        <h1 className="search-title"> Awesome Exercice You Should Know</h1>
        <div className="search-cont">
          <input
            className="search-input"
            type="text"
            value={search}
            onChange={(e) => setSearch(e.target.value.toLowerCase())}
            placeholder="Search Exercices"
            onClick={handleSearch}
          />
          <button className="search-btn">Search</button>
        </div>
      </div>
      <div className="scroll-hor-cont"></div>
    </>
  );
};

export default Exercice;
