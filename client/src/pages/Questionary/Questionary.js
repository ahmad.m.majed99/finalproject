import React, { useEffect, useState } from "react";
import "./Questionary.css";
import axios from "axios";
import { GrFormPreviousLink } from "react-icons/gr";
import { FiCheck } from "react-icons/fi";

import { saveAs } from "file-saver";

import Navbar from "../../components/Navbar/Navbar";
import Gender from "../../media/images/gender.jpg";
import FemaleGender from "../../media/images/femaleGender.png";
import MaleGender from "../../media/images/maleGender.png";

import { RiDownloadLine } from "react-icons/ri";

import ProgressBar from "@ramonak/react-progress-bar";

import { RiErrorWarningLine } from "react-icons/ri";

var i = 1;

const Questionary = () => {
  const [slide, setSlide] = useState(1);
  const [data, setData] = useState({});
  const [Err, setErr] = useState();
  const [valid, setValid] = useState(false);
  const [valid1, setValid1] = useState(false);
  const [program, setProgram] = useState();
  const [imgurl, setImageurl] = useState();
  const [loader, setLoader] = useState(false);

  const token = localStorage.getItem("token");

  const handleResult = async () => {
    try {
      const response = await axios.post(
        "http://localhost:5000/api/users/questionary",
        data
      );

      console.log(response);
      setLoader(true);
      setProgram(response);
      setImageurl(response.data?.program);
      console.log(response.data.program);
      setTimeout(() => {
        i++;
        setSlide(i);
      }, 600);
    } catch (err) {
      console.log(err);
    }
  };

  useEffect(() => {
    handleResult();
  }, []);

  const downloadImage = () => {
    saveAs(`http://localhost:5000/${imgurl}`, "image.png");
  };

  function handleChange(e) {
    const Val = e.target.value;
    if (Val > 140 && Val < 200) {
      setErr(0);
      setValid(true);
      setData((prev) => ({ ...prev, [e.target.name]: e.target.value }));
      console.log(Val);
    } else {
      setErr(1);
      setValid(false);
      console.log("error err");
      console.log(e.target.name, e.target.value);
    }
  }

  function handleChange1(e) {
    const Val = e.target.value;
    if (Val > 30 && Val < 140) {
      setErr(0);
      setValid1(true);
      setData((prev) => ({ ...prev, [e.target.name]: e.target.value }));
      console.log(Val);
    } else {
      setErr(1);
      setValid1(false);
      console.log("error err");
      console.log(e.target.name, e.target.value);
    }
  }

  function handleClick(e) {
    if (i >= 1 && i <= 8) {
      setTimeout(() => {
        i++;
        setSlide(i);
        console.log(i);
        console.log(e.target.name, e.target.value);
        console.log(data);
      }, 600);
      if (i == 1) {
        setData({ [e.target.name]: e.target.value });
      } else if (i != 4 && i != 5) {
        setData((prev) => ({ ...prev, [e.target.name]: e.target.value }));
      }

      console.log("✅ Checkbox is checked");
    } else {
      console.log("⛔️ Checkbox is NOT checked");
    }
    // setIsSubscribed((current) => !current);
  }

  function handlePrev() {
    if (i >= 0) {
      i--;
      setValid(false);
      setValid1(false);
      setSlide(i);
      console.log("prev function clicked succefully");
    }
  }

  return (
    <>
      <div>
        <Navbar />
      </div>
      {slide != 1 && slide != 8 && slide != 9 ? (
        <ProgressBar
          className="progressBar"
          completed={slide - 1}
          maxCompleted={8}
          height="0.6vh"
          baseBgColor="#191919"
          bgColor="#f42f64"
          transitionDuration="0.3s"
          labelClassName="bar__label"
        />
      ) : null}

      {slide === 1 ? (
        <div className="gender-container">
          <h5>Take 1-minute quiz</h5>
          <h2>Personal Plan</h2>
          <p className="gender-parag">according to your age and BMI</p>
          <h5>Select your gender</h5>
          <div className="gender-cont">
            <div className="male-gend">
              <label className="checkbox-gender">
                <img src={MaleGender} alt="gender" />
                <input
                  type="checkbox"
                  name="gender"
                  value="Male"
                  className="male-input"
                  onChange={handleClick}
                />
              </label>
              <br />
              Male
              <span className="checkmark1">
                <FiCheck className="check-icon" />
              </span>
            </div>
            <div className="female-gend">
              <label className="checkbox-gender">
                <img src={FemaleGender} alt="gender" className="femaleGender" />
                <input
                  type="checkbox"
                  name="gender"
                  value="Female"
                  className="female-input"
                  onChange={handleClick}
                />
              </label>
              <br />
              Female
              <span className="checkmark1">
                <FiCheck className="check-icon" />
              </span>
            </div>
          </div>
        </div>
      ) : null}
      {slide === 2 ? (
        <div className="slide current">
          <div className="questionary-images">
            <img src={Gender} alt="gender" />
          </div>
          <div className="questionary-container">
            <div className="question-form">
              <div className="sliderInfo">
                <GrFormPreviousLink
                  style={{ backgroundColor: "#fff" }}
                  onClick={handlePrev}
                  className="prev-icon"
                />
                <div className="slideNumb">{slide}/8</div>
              </div>

              <h1 className="questionary-title">What is your primary goal?</h1>

              <label className="checkbox-container">
                Lose Weight
                <p className="checbox-desc">
                  Start torching calories and melting fat
                </p>
                <input
                  type="checkbox"
                  name="goal"
                  value="LoseWeight"
                  onChange={handleClick}
                />
                <span className="checkmark">
                  <FiCheck className="check-icon" />
                </span>
              </label>
              <label className="checkbox-container">
                Gain Muscles
                <p className="checbox-desc">
                  Force your muscles to grow faster
                </p>
                <input
                  type="checkbox"
                  name="goal"
                  value="GainMuscles"
                  onChange={handleClick}
                />
                <span className="checkmark">
                  <FiCheck className="check-icon" />
                </span>
              </label>
              <label className="checkbox-container">
                Fit Body
                <p className="checbox-desc">
                  Say no to food cravings and become a healthy eater
                </p>
                <input
                  type="checkbox"
                  name="goal"
                  value="FitBody"
                  onChange={handleClick}
                />
                <span className="checkmark">
                  <FiCheck className="check-icon" />
                </span>
              </label>
            </div>
          </div>
        </div>
      ) : null}
      {slide === 3 ? (
        <div className="slide current">
          <div className="questionary-images">
            <img src={Gender} alt="gender" />
          </div>
          <div className="questionary-container">
            <div className="question-form">
              <div className="sliderInfo">
                <GrFormPreviousLink
                  style={{ backgroundColor: "#fff" }}
                  onClick={handlePrev}
                  className="prev-icon"
                />
                <div className="slideNumb">{slide}/8</div>
              </div>
              <h1 className="questionary-title">What is your age?</h1>

              <label className="checkbox-container">
                20y
                <input
                  type="checkbox"
                  name="age"
                  value="20"
                  onClick={handleClick}
                />
                <span className="checkmark1">
                  <FiCheck className="check-icon" />
                </span>
              </label>
              <label className="checkbox-container">
                30y
                <input
                  type="checkbox"
                  name="age"
                  value="31"
                  onClick={handleClick}
                />
                <span className="checkmark1">
                  <FiCheck className="check-icon" />
                </span>
              </label>
              <label className="checkbox-container">
                40y
                <input
                  type="checkbox"
                  name="age"
                  value="41"
                  onClick={handleClick}
                />
                <span className="checkmark1">
                  <FiCheck className="check-icon" />
                </span>
              </label>
              <label className="checkbox-container">
                50y+
                <input
                  type="checkbox"
                  name="age"
                  value="51"
                  onChange={handleClick}
                />
                <span className="checkmark1">
                  <FiCheck className="check-icon" />
                </span>
              </label>
            </div>
          </div>
        </div>
      ) : null}
      {slide === 4 ? (
        <div className="slide current ">
          <div className="questionary-images">
            <img src={Gender} alt="gender" />
          </div>
          <div className="questionary-container">
            <div className="question-form">
              <div className="sliderInfo">
                <GrFormPreviousLink
                  style={{ backgroundColor: "#fff" }}
                  onClick={handlePrev}
                  className="prev-icon"
                />
                <div className="slideNumb">{slide}/8</div>
              </div>
              <div className="tall1-cont">
                <h1 className="questionary-title">How tall are you?</h1>
                <div className="tall-cont1">
                  <div className="tall-cont">
                    <input
                      type="number"
                      min="140"
                      max="200"
                      name="height"
                      className="tall-input"
                      onChange={handleChange}
                    />
                    <div className="tall-span">CM</div>
                  </div>

                  {Err === 1 ? (
                    <div className="err-cont">
                      <RiErrorWarningLine className="Err-icon" />
                      <div className="heightErr">
                        PLease enter a valide number
                      </div>
                    </div>
                  ) : null}
                </div>

                <button
                  disabled={!valid ? true : false}
                  className="tall-btn"
                  onClick={handleClick}
                >
                  Next Step
                </button>
              </div>
            </div>
          </div>
        </div>
      ) : null}
      {slide === 5 ? (
        <div className="slide current ">
          <div className="questionary-images">
            <img src={Gender} alt="gender" />
          </div>
          <div className="questionary-container">
            <div className="question-form">
              <div className="sliderInfo">
                <GrFormPreviousLink
                  style={{ backgroundColor: "#fff" }}
                  onClick={handlePrev}
                  className="prev-icon"
                />
                <div className="slideNumb">{slide}/8</div>
              </div>
              <div className="tall1-cont">
                <h1 className="questionary-title">
                  What's your current weight?
                </h1>
                <div className="tall"></div>
                <div className="tall-cont1">
                  <div className="tall-cont">
                    <input
                      type="number"
                      min="140"
                      max="200"
                      name="weight"
                      onChange={handleChange1}
                      className="tall-input"
                    />
                    <div className="tall-span">KG</div>
                  </div>
                  {Err === 1 ? (
                    <div className="err-cont">
                      <RiErrorWarningLine className="Err-icon" />
                      <div className="heightErr">
                        PLease enter a valide number
                      </div>
                    </div>
                  ) : null}
                </div>

                <button
                  disabled={!valid1 ? true : false}
                  className="tall-btn"
                  onClick={handleClick}
                >
                  Next Step
                </button>
              </div>
            </div>
          </div>
        </div>
      ) : null}
      {slide === 6 ? (
        <div className="slide current">
          <div className="questionary-images">
            <img src={Gender} alt="gender" />
          </div>
          <div className="questionary-container">
            <div className="question-form">
              <div className="sliderInfo">
                <GrFormPreviousLink
                  style={{ backgroundColor: "#fff" }}
                  onClick={handlePrev}
                  className="prev-icon"
                />
                <div className="slideNumb">{slide}/8</div>
              </div>
              <h1 className="questionary-title">
                Please, describe your daily routine
              </h1>

              <label className="checkbox-container">
                At the office
                <input
                  type="checkbox"
                  name="sport"
                  value="noTrain"
                  onClick={handleClick}
                />
                <span className="checkmark1">
                  <FiCheck className="check-icon" />
                </span>
              </label>
              <label className="checkbox-container">
                Daily long walks
                <input
                  type="checkbox"
                  name="sport"
                  value="noTrain"
                  onClick={handleClick}
                />
                <span className="checkmark1">
                  <FiCheck className="check-icon" />
                </span>
              </label>
              <label className="checkbox-container">
                Physical work
                <input
                  type="checkbox"
                  name="sport"
                  value="train"
                  onClick={handleClick}
                />
                <span className="checkmark1">
                  <FiCheck className="check-icon" />
                </span>
              </label>
              <label className="checkbox-container">
                Mostly at home
                <input
                  type="checkbox"
                  name="sport"
                  value="noTrain"
                  onClick={handleClick}
                />
                <span className="checkmark1">
                  <FiCheck className="check-icon" />
                </span>
              </label>
            </div>
          </div>
        </div>
      ) : null}
      {slide === 7 ? (
        <div className="slide current">
          <div className="questionary-images">
            <img src={Gender} alt="gender" />
          </div>
          <div className="questionary-container">
            <div className="question-form">
              <div className="sliderInfo">
                <GrFormPreviousLink
                  style={{ backgroundColor: "#fff" }}
                  onClick={handlePrev}
                  className="prev-icon"
                />
                <div className="slideNumb">{slide}/8</div>
              </div>
              <h1 className="questionary-title">Choose your diet type</h1>
              <label className="checkbox-container">
                Traditional
                <p className="checbox-desc">I enjoy everything</p>
                <input
                  type="checkbox"
                  name="food"
                  value="notVegan"
                  onClick={handleClick}
                />
                <span className="checkmark">
                  <FiCheck className="check-icon" />
                </span>
              </label>
              <label className="checkbox-container">
                Vegeterian
                <p className="checbox-desc">I avoid meat and fish</p>
                <input
                  type="checkbox"
                  name="food"
                  value="vegan"
                  onClick={handleClick}
                />
                <span className="checkmark">
                  <FiCheck className="check-icon" />
                </span>
              </label>
            </div>
          </div>
        </div>
      ) : null}
      {slide === 8 ? (
        <div className="slide current">
          <div className="questionary-container-result">
            <div className="question-form-result">
              <h2 className="prog-title">Enter your email to get your plan</h2>
              <input
                className="input-email"
                type="email"
                placeholder="Enter your email to get your plan"
                required
              />
              <br />

              <p className="privacy-parag">
                <i className="fa fa-lock" aria-hidden="true"></i>
                We respect your privacy and are committed to protecting your
                personal data. We’ll email you a copy of your results for
                convenient access.
              </p>
              <label className="checkbox-container-result">
                <button
                  type="checkbox"
                  onClick={handleResult}
                  className="plan-btn"
                >
                  Get my plan
                </button>
                <span className="checkmark">
                  <FiCheck className="check-icon" />
                </span>
              </label>
            </div>
          </div>
        </div>
      ) : null}
      {slide === 9 ? (
        <div className="slide current">
          <div className="plan-cont">
            <div className="plan-details">
              <h3>Your bmi is: {program.data?.BMI}</h3>
              <h3>Your type: {program.data?.type}</h3>
            </div>
            <div className="plan-program">
              <h1 className="title-progr">
                Your program that suits you is ready !
              </h1>
              <h2>Download your Program</h2>
              <button className="download-btn">
                <RiDownloadLine
                  className="download-icon"
                  onClick={downloadImage}
                />
              </button>
            </div>
          </div>
        </div>
      ) : null}
    </>
  );
};

export default Questionary;
