import React from "react";
import "./Home.css";

import Navbar from "../../components/Navbar/Navbar";
import Footer from "../../components/Footer/Footer";
import Program from "../Program/Program";
import Community from "../Community/Community";
import Fitness from "../../media/images/fitness.jpg";
import { RiHeartPulseFill } from "react-icons/ri";
import { BsFillPlayCircleFill } from "react-icons/bs";

import { useNavigate } from "react-router-dom";

const Home = () => {
  let navigate = useNavigate();
  const routeQuestionary = () => {
    let path = `questionary`;
    navigate(path);
  };

  return (
    <>
      <div className="nav-comp">
        <Navbar />
      </div>

      <div className="home-container" id="home">
        <div className="left-container">
          <div className="join-membership">
            <button className="join-btn">JOIN</button>
            <span className="join-txt">
              Get more benefit by joining membership
            </span>
          </div>
          <div className="wrapper-home">
            <h1 className="home-title">HELPS FOR IDEAL BODY FITNESS</h1>
            <h6 className="home-prg">
              Want your body to be healty, join our program with directions
              according to your body's goals
            </h6>
            <div className="wrapper-btn">
              <button className="start-btn" onClick={routeQuestionary}>
                Start Now
              </button>
              <button className="demo-btn">
                <BsFillPlayCircleFill />
                Demo Training
              </button>
            </div>
            <div className="wrapper-details">
              <div className="details-ex">
                <div className="ex-nb">80</div>
                <div className="ex-prg">
                  Exercice <br /> program
                </div>
              </div>
              <div className="details-memb">
                <div className="memb-nb">872+</div>
                <div className="memb-prg">Members</div>
              </div>
              <div className="details-coach">
                <div className="coach-nb">120+</div>
                <div className="coach-prg">Total coach</div>
              </div>
            </div>
          </div>
        </div>
        <div className="right-container">
          <h1 className="right-title">FITNESS</h1>
          <img src={Fitness} alt="fitness" className="home-img" />
          <div className="rate-ctn">
            <div className="rate-icon">
              <RiHeartPulseFill className="heart-icon" />
            </div>

            <p className="rate-p">Heart Rate</p>
            <div className="rate-nb">2245 bpm</div>
          </div>
          <div className="cal-ctn">
            <p className="cal-p">Daily calorie burn</p>
            <div className="cal-nb">42 Cal</div>
            <div className="cal-graph">
              <div className="g1"></div>
              <div className="g2"></div>
              <div className="g3"></div>
              <div className="g4"></div>
              <div className="g5"></div>
            </div>
          </div>
        </div>
      </div>
      <div className="prog-comp">
        <Program />
      </div>
      <div className="com-comp">
        <Community />
      </div>
      <div className="footer-comp">
        <Footer />
      </div>
    </>
  );
};

export default Home;
