import React from "react";
import "./Community.css";
import { FiCheck } from "react-icons/fi";
import Fitness1 from "../../media/images/fitness1.jpg";
import Fitness2 from "../../media/images/fitness2.jpg";
import Fitness3 from "../../media/images/fitness3.jpg";

const Community = () => {
  return (
    <div className="community-container" id="community">
      <div className="com-info">
        <div className="com-title">
          <div className="com-title1">WHY JOIN US</div>
          <div className="com-title2">JOIN</div>
        </div>

        <div className="com-details">
          <div className="com-icon">
            <FiCheck />
          </div>
          <p>We have proffesional trainer</p>
        </div>
        <div className="com-details">
          <div className="com-icon">
            <FiCheck />
          </div>
          <p>Free join community</p>
        </div>
        <div className="com-details">
          <div className="com-icon">
            <FiCheck />
          </div>
          <p>1free program for new members</p>
        </div>
      </div>
      <div className="com-img">
        <img src={Fitness1} alt="fitness1" className="fit1" />
        <img src={Fitness2} alt="fitness2" className="fit2" />
        <img src={Fitness3} alt="fitness3" className="fit3" />
      </div>
    </div>
  );
};

export default Community;
